import os
import sys

from django.core.management.base import BaseCommand

from innsmouth.data_pipeline.models import Vehicle
from innsmouth.data_pipeline.serializers import VehicleSerializer
from innsmouth.data_pipeline.services import MetrobusService


class Command(BaseCommand):
    def handle(self, *args, **options):
        print(os.environ)

        try:
            metrobus_service = MetrobusService()
            print("obtain metrobus service")

            metrobus_data = metrobus_service.json
            print("obtain metrobus data")

            if isinstance(metrobus_data, dict):
                metrobus_data_serializer = VehicleSerializer(data=metrobus_data)
                metrobus_data_serializer.is_valid(raise_exception=True)
                metrobus_data_serializer.save()

            elif isinstance(metrobus_data, list):
                metrobus_data_serializer = VehicleSerializer(data=metrobus_data, many=True)
                metrobus_data_serializer.is_valid(raise_exception=True)
                metrobus_data_serializer.save()

            print("The data was inserted successfully")
        except Exception as e:
            print(e)
