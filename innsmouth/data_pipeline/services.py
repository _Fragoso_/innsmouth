from dataclasses import dataclass
import requests
from utils.geolocation_handlers import GeoUtils
from numba import jit

METROBUS_DATA_URL = (
    "https://datos.cdmx.gob.mx/api/records/1.0/search/?dataset=prueba_fetchdata_metrobus"
)


class MetrobusService:
    """
    Class that consumes the API of the vehicle units,
    and then create the structure of each vehicle record 
    to save it into the database.
    """
    def __init__(self):
        self.url = METROBUS_DATA_URL

    @property
    def _max_number_of_records(self) -> int:
        """
        Property method that obtains the maximum
        number of records per hour.

        Returns
        -------
        int
            Maximum number of records per hour.
        """
        return requests.get(self.url).json().get("nhits")

    def _obtain_records(self) -> list or None:
        """
        Method that obtains all the records per hour.

        Returns
        -------
        list or None
            The vehicle units records returned by the API.
        """
        number_of_records = self._max_number_of_records

        try:
            records = (
                requests.get(self.url, params={"rows": number_of_records})
                .json()
                .get("records")
            )
            return records
        except Exception:
            return None

    @property
    def json(self) -> dict or list:
        """
        Property method that returns a formatted
        list or dict of records that are going to be saved inside
        the database.

        Returns
        -------
        dict or list
            The formatted records that are going to be saved.
        """
        records_obtained = self._obtain_records()

        if len(records_obtained) == 1:
            return self._create_custom_dict(records_obtained[0])

        return [self._create_custom_dict(record["fields"]) for record in records_obtained]

    def _create_custom_dict(self, record_fields: dict) -> dict:
        """
        Private method that returns a custom dict
        based on the structure of the Vehice mapped to 
        the database table.

        Parameters
        ----------
        record_fields: dict
            The data of the actual record óbtained.
        
        Returns
            dict
                The formatted record.
        """

        return {
            "vehicle_id": record_fields["vehicle_id"],
            "latitude": record_fields["position_latitude"],
            "longitude": record_fields["position_longitude"],
            "status": record_fields["vehicle_current_status"],
            "date_updated": record_fields["date_updated"],
            "town_hall": GeoUtils.obtain_town_hall_id(
                record_fields["position_latitude"], record_fields["position_longitude"]
            ),
        }
