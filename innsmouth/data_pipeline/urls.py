from django.conf.urls import url
from .views import available_units, unit_detail, available_town_halls, town_hall_detail
from django.urls import path

urlpatterns = [
    path(r"available_units", available_units),
    path(r"unit_detail/<int:pk>", unit_detail),
    path(r"available_town_halls", available_town_halls),
    path(r"town_hall_detail/<int:pk>", town_hall_detail),
]
