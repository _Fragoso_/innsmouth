from django.db import models


class TownHall(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        db_table = "town_halls"


class Vehicle(models.Model):
    vehicle_id = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()
    status = models.IntegerField()
    date_updated = models.DateTimeField()
    town_hall = models.ForeignKey(TownHall, on_delete=models.CASCADE, null=True)

    class Meta:
        db_table = "vehicles"
