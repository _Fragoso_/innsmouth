from rest_framework import serializers

from .models import Vehicle, TownHall


class VehicleSerializer(serializers.ModelSerializer):
    """
    Serializer to save the data of a vehicle.
    """

    class Meta:
        model = Vehicle
        fields = "__all__"


class AvailableVehicleSerializer(serializers.ModelSerializer):
    """
    Serializer to obtain the ids of all the availables vehicles.
    """

    class Meta:
        model = Vehicle
        fields = ["vehicle_id"]


class VehicleDetailSerializer(serializers.ModelSerializer):
    """
    Serializer to obtain the some specific details of a vehicle.
    """

    location = serializers.SerializerMethodField()
    town_hall = serializers.SerializerMethodField()
    date = serializers.DateTimeField(source="date_updated")

    class Meta:
        model = Vehicle
        fields = ["location", "town_hall", "date"]

    def get_location(self, obj):
        return {
            "latitude": obj.latitude,
            "longitude": obj.longitude,
        }

    def get_town_hall(self, obj):
        return obj.town_hall.name


class TownHallSerializer(serializers.ModelSerializer):
    """
    Serializer to obtain al the town halls and his names. 
    """

    town_hall_id = serializers.PrimaryKeyRelatedField(source="id", read_only=True)

    class Meta:
        model = TownHall
        fields = ["town_hall_id", "name"]


class TownHallDetailSerializer(serializers.ModelSerializer):
    """
    Serializer to obtain one town hall and the vehicles inside him.
    """

    town_hall_id = serializers.PrimaryKeyRelatedField(source="id", read_only=True)
    vehicle_units = serializers.SerializerMethodField()

    class Meta:
        model = TownHall
        fields = ["town_hall_id", "name", "vehicle_units"]

    def get_vehicle_units(self, obj):
        return obj.vehicle_set.distinct().values("vehicle_id")
