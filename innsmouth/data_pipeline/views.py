from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import TownHall, Vehicle
from .serializers import (
    AvailableVehicleSerializer,
    TownHallDetailSerializer,
    TownHallSerializer,
    VehicleDetailSerializer,
    VehicleSerializer,
)


@api_view(["GET"])
def available_units(request, *args, **kwargs):
    """
    View method that obtains all the available
    vehicle units filtered by his status.

    Parameters
    ----------
    request: `rest_framework.request.Request` object. 
        HTTP Request sended to the server.
   
    Returns
    -------
    `rest_framework.response.Response`
        HTTP Response that contains the available vehicle units.
    """
    if request.method == "GET":
        units_obtained = Vehicle.objects.filter(status=1)
        units_serializer = AvailableVehicleSerializer(units_obtained, many=True)

        return Response(
            data={
                "message": "The units were obtained successfully",
                "available_units": units_serializer.data,
            },
            status=status.HTTP_302_FOUND,
        )


@api_view(["GET"])
def unit_detail(request, pk, *args, **kwargs):
    """
    View method that obtains a vehicle unit based on
    his `vehicle_id` field.

    Parameters
    ----------
    request: `rest_framework.request.Request` object. 
        HTTP Request sended to the server.
    
    pk: int
        `vehicle_id` of the vehicle unit that is
        going to be requested.

    Returns
    -------
    `rest_framework.response.Response`
        HTTP Response that contains the requested vehicle unit
        with his historial of locations.
    """

    if request.method == "GET":
        filtered_vehicles = Vehicle.objects.filter(vehicle_id=pk)

        if len(filtered_vehicles) == 0:
            return Response(
                data={"message": "The vehicle requested doesn't exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

        filtered_vehicles_serializer = VehicleDetailSerializer(
            filtered_vehicles, many=True
        )

        return Response(
            data={
                "message": "The vehicle was obtained successfully",
                "data": {
                    "vehicle_id": pk,
                    "historial": filtered_vehicles_serializer.data,
                },
            },
            status=status.HTTP_302_FOUND,
        )


@api_view(["GET"])
def available_town_halls(request, *args, **kwargs):
    """
    View method that obtains all the town halls
    inside the database.

    Parameters
    ----------
    request: `rest_framework.request.Request` object. 
        HTTP Request sended to the server.
    
    Returns
    -------
    `rest_framework.response.Response`
        HTTP Response that contains all the town halls.
    """
    town_halls_obtained = TownHall.objects.all()

    town_halls_serializer = TownHallSerializer(town_halls_obtained, many=True)

    return Response(
        data={
            "message": "The town halls were obtained successfully",
            "data": town_halls_serializer.data,
        }
    )


def obtain_town_hall(pk: int) -> object or None:
    """
    Method that returns the respective town hall 
    instance in case it exists, otherwise it
    returns None.

    Parameters
    ----------
    pk: int
        The id of the town hall to be requested.
    
    Returns
    -------
    `TownHall` object or `None`
        The TownHall instance, or a None object
        in case the instance don't exist.
    """
    try:
        town_hall_obtained = TownHall.objects.get(pk=pk)
        return town_hall_obtained
    except Exception:
        return None


@api_view(["GET"])
def town_hall_detail(request, pk, *args, **kwargs):
    """
    View method that obtains a town hall based on his id.

    Parameters
    ----------
    request: `rest_framework.request.Request` object. 
        HTTP Request sended to the server.
    
    pk: int
        ID of the town hall that is going to be requested.

    Returns
    -------
    `rest_framework.response.Response`
        HTTP Response that contains the town hall requested
        with all the vehicles that are actually in there.
    """
    
    if request.method == "GET":
        town_hall_obtained = obtain_town_hall(pk)

        if town_hall_obtained is None:
            return Response(
                data={"message": "The town hall wasn't found"},
                status=status.HTTP_404_NOT_FOUND,
            )

        town_hall_serializer = TownHallDetailSerializer(town_hall_obtained)

        return Response(
            {
                "message": "The units inside a town hall were obtained successfuly",
                "data": town_hall_serializer.data,
            }
        )
