
TOWN_HALL_NAMES = {
    "Álvaro Obregón": 1,
    "Azcapotzalco": 2,
    "Benito Juárez": 3,
    "Coyoacán": 4,
    "Cuajimalpa de Morelos": 5,
    "Cuauhtémoc": 6,
    "Gustavo A. Madero": 7,
    "Iztacalco": 8,
    "Iztapalapa": 9,
    "La Magdalena Contreras": 10,
    "Miguel Hidalgo": 11,
    "Milpa Alta": 12,
    "Tláhuac": 13,
    "Tlalpan": 14,
    "Venustiano Carranza": 15,
    "Xochimilco": 16,
}