PROJECT_DIRECTORY=$1;

obtain_env_variable() {
    ENV_VARIABLE=$(grep $1 $PROJECT_DIRECTORY/cron_env_variables/.env | xargs)
    ENV_VARIABLE=${ENV_VARIABLE#*=}
    echo $ENV_VARIABLE
}

POSTGRESQL_DB_USER=$(obtain_env_variable POSTGRESQL_DB_USER)
POSTGRESQL_DB_PASSWORD=$(obtain_env_variable POSTGRESQL_DB_PASSWORD)
POSTGRESQL_DB_NAME=$(obtain_env_variable POSTGRESQL_DB_NAME)
POSTGRESQL_DB_HOST=$(obtain_env_variable POSTGRESQL_DB_HOST)
POSTGRESQL_DB_PORT=$(obtain_env_variable POSTGRESQL_DB_PORT)

cd $PROJECT_DIRECTORY;

VIRTUAL_ENV_PYTHON_PATH=$(pipenv --py)

env POSTGRESQL_DB_USER=$POSTGRESQL_DB_USER \
    POSTGRESQL_DB_PASSWORD=$POSTGRESQL_DB_PASSWORD \
    POSTGRESQL_DB_NAME=$POSTGRESQL_DB_NAME \
    POSTGRESQL_DB_HOST=$POSTGRESQL_DB_HOST \
    POSTGRESQL_DB_PORT=$POSTGRESQL_DB_PORT \
    PYTHONIOENCODING=UTF-8 \
    PYTHONUNBUFFERED=1 \
    $VIRTUAL_ENV_PYTHON_PATH $PROJECT_DIRECTORY/manage.py adding_data >> $PROJECT_DIRECTORY/innsmouth/data_pipeline/display_data_example.txt
