from geopy import Nominatim
from innsmouth.data_pipeline.constants import TOWN_HALL_NAMES
from geopy.exc import GeocoderTimedOut, GeocoderServiceError
from numba import jit

geolocator = Nominatim(user_agent="custom_geolocator")


class GeoUtils:
    """
    Static class that obtains the respective `town hall id` 
    of the vehicle based in his longitude and latitude.
    """

    @classmethod
    def obtain_town_hall_id(self, latitude: float, longitude: float) -> int or None:
        """
        Method that obtains the `town hall id` based in the position
        of the respective vehicle.

        Parameters
        ----------
        latitude: float
            The latitude position of the vehicle.
        
        longitude: float
            The longitude position of the vehicle.
        
        Returns
        -------
        int or None
            The id of the respective town hall.
        """
        town_hall_id_obtained: int = None

        address = self._obtain_address(self, latitude, longitude)

        for town_hall_name, town_hall_id in TOWN_HALL_NAMES.items():
            if address is None:
                town_hall_id_obtained = None
                break

            if town_hall_name in address:
                town_hall_id_obtained = town_hall_id
                break

        return town_hall_id_obtained

    def _obtain_address(self, latitude: float, longitude: float) -> str:
        """
        Method that returns an string address based on the 
        latitude and longitude entered.

        Parameters
        ----------
        latitude: float
            The latitude position of the vehicle.
        
        longitude: float
            The longitude position of the vehicle.
        
        Returns
        -------
        str
            The location address of the vehicle.
        """
        try:
            return geolocator.reverse(",".join([str(latitude), str(longitude)])).address
        except Exception as e:
            return self._obtain_address(self, latitude, longitude)
