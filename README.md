# Innsmouth
**Innsmouth** is a Django application that consumes the **`CDMX METROBUS API`** and filter the data obtained based on the **vehicle units status** and in the **town halls** that contains some vehicles.

## Dependencies
![Python +3.7](https://img.shields.io/badge/python-+3.7-blue.svg)
![Docker](https://img.shields.io/badge/docker-19.03.8-blue.svg)
![PostgreSQL](https://img.shields.io/badge/postgres-*-blue.svg)

## Configuration steps

Clone the repository
```bash
> git clone git@bitbucket.org:_Fragoso_/innsmouth.git
```

Then inside the project directory, execute the following command.
```
cp .env.example .env
```

This command create the `.env` file, that will hold the environment variables containing the private access credentials of the database server.

The variables contained inside the `.env` file are listed below.

### **Environment variables**
| Name                   | Value        |
|------------------------|--------------|
| POSTGRESQL_DB_USER     | postgres     |
| POSTGRESQL_DB_PASSWORD | password     |
| POSTGRESQL_DB_NAME     | innsmouth_db |
| POSTGRESQL_DB_HOST     | 127.0.0.1    |
| POSTGRESQL_DB_PORT     | 50780        |

Create the `PostgreSQL` database server with the commands listed below.
```bash
> docker network create \ 
--driver=bridge innsmouth_network

> docker run -d --name innsmouth_postgres \
-v innsmouth_volume=/var/lib/postgresql/data \
--network=innsmouth_network \
-e POSTGRES_PASSWORD=password -p 50780:5432 postgres
```

Create the `innsmouth_db` database.
```bash
> docker exec -it innsmouth_postgres bash

> psql -U postgres

> CREATE DATABASE innsmouth_db;

> GRANT ALL PRIVILEGES ON DATABASE innsmouth_db TO postgres;
```

Go back to the root project directory, and execute the following commands to activate the environment, create the tables structure and populate de database.

```bash
> pipenv shell --fancy
> python3 manage.py migrate
> python3 manage.py loaddata town_halls.json
> python3 manage.py loaddata vehicles.json   
```

Configure the `cron jobs` and run the application.

Open the `crontab` editor.

```bash
> crontab -e
```

Add the following line inside the `crontab` editor and save it.

```bash
> */10 * * * * /usr/bin/sh /<innsmouth_project_absolute_path>/running_cronjob.sh
```

Run the application.

```bash
> python3 manage.py runserver
```

Now you have the application running inside your localhost.

If you want to run the Django application inside a Docker container, you want to execute the following commands inside
the project root directory, after you have modified the
following `.env` variables with the following values.

`POSTGRESQL_DB_HOST=innsmouth_postgres`
`POSTGRESQL_DB_PORT=5432`

```bash
> docker build -f Dockerfile  -t innsmouth:1.0 .

> docker run --restart=always \
--name=innsmouth_project \
--link=innsmouth_postgres:postgres \ --network=innsmouth_network \
--env-file .env -p 8095:7070 -d innsmouth:1.0
```

# Results

## **Obtain available units**
```
http://127.0.0.1:8095/data_pipeline/available_units
```

**Response**

```json
{
    "message": "The units were obtained successfully",
    "available_units": [
        {
            "vehicle_id": "177"
        },
        {
            "vehicle_id": "1302"
        },
        {
            "vehicle_id": "371"
        },
        {
            "vehicle_id": "375"
        },
        {
            "vehicle_id": "416"
        },
        {
            "vehicle_id": "28"
        },       
        {
            "vehicle_id": "497"
        }
    ]
}
```

## **Obtain unit detail**
```
http://127.0.0.1:8095/data_pipeline/unit_detail/1044
```

**Response**
```json
{
    "message": "The vehicle was obtained successfully",
    "data": {
        "vehicle_id": 1044,
        "historial": [
            {
                "location": {
                    "latitude": 19.364599227905273,
                    "longitude": -99.18199920654297
                },
                "town_hall": "Benito Juárez",
                "date": "2020-05-21T00:00:10Z"
            },
            {
                "location": {
                    "latitude": 19.364599227905273,
                    "longitude": -99.18199920654297
                },
                "town_hall": "Benito Juárez",
                "date": "2020-05-21T01:01:09Z"
            },
            {
                "location": {
                    "latitude": 19.364599227905273,
                    "longitude": -99.18199920654297
                },
                "town_hall": "Benito Juárez",
                "date": "2020-05-21T01:01:09Z"
            },
            {
                "location": {
                    "latitude": 19.364599227905273,
                    "longitude": -99.18199920654297
                },
                "town_hall": "Benito Juárez",
                "date": "2020-05-21T01:01:09Z"
            }
        ]
    }
}
```

## **Obtain available town halls**
```
http://127.0.0.1:8095/data_pipeline/available_town_halls
```

**Response**
```json
{
    "message": "The town halls were obtained successfully",
    "data": [
        {
            "town_hall_id": 1,
            "name": "Álvaro Obregón"
        },
        {
            "town_hall_id": 2,
            "name": "Azcapotzalco"
        },
        {
            "town_hall_id": 3,
            "name": "Benito Juárez"
        },
        {
            "town_hall_id": 4,
            "name": "Coyoacán"
        },
        {
            "town_hall_id": 5,
            "name": "Cuajimalpa de Morelos"
        },
        {
            "town_hall_id": 6,
            "name": "Cuauhtémoc"
        },
        {
            "town_hall_id": 7,
            "name": "Gustavo A. Madero"
        },
        {
            "town_hall_id": 8,
            "name": "Iztacalco"
        },
        {
            "town_hall_id": 9,
            "name": "Iztapalapa"
        },
        {
            "town_hall_id": 10,
            "name": "La Magdalena Contreras"
        },
        {
            "town_hall_id": 11,
            "name": "Miguel Hidalgo"
        },
        {
            "town_hall_id": 12,
            "name": "Milpa Alta"
        },
        {
            "town_hall_id": 13,
            "name": "Tláhuac"
        },
        {
            "town_hall_id": 14,
            "name": "Tlalpan"
        },
        {
            "town_hall_id": 15,
            "name": "Venustiano Carranza"
        },
        {
            "town_hall_id": 16,
            "name": "Xochimilco"
        }
    ]
}
```

## *Obtain town hall detail*
```
http://127.0.0.1:8095/data_pipeline/town_hall_detail/3
```

**Response**
```json
{
    "message": "The units inside a town hall were obtained successfuly",
    "data": {
        "town_hall_id": 3,
        "name": "Benito Juárez",
        "vehicle_units": [
            {
                "vehicle_id": "1070"
            },
            {
                "vehicle_id": "992"
            },
            {
                "vehicle_id": "39"
            },
            {
                "vehicle_id": "1235"
            },
            {
                "vehicle_id": "1232"
            },
            {
                "vehicle_id": "10015"
            },
            {
                "vehicle_id": "51"
            },
            {
                "vehicle_id": "178"
            },
            {
                "vehicle_id": "450"
            },
            {
                "vehicle_id": "1237"
            },
            {
                "vehicle_id": "1044"
            },
            {
                "vehicle_id": "1241"
            },
            {
                "vehicle_id": "1253"
            },
            {
                "vehicle_id": "988"
            },
            {
                "vehicle_id": "1288"
            },
            {
                "vehicle_id": "3"
            },
            {
                "vehicle_id": "1024"
            }
        ]
    }
}
```

